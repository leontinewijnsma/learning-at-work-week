provider "aws" {
  region = "eu-west-2"
  access_key = var.AWS_ACCESS_KEY_ID
  secret_key = var.AWS_SECRET_ACCESS_KEY
}


terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.75.2"
    }
  }
}

variable "AWS_ACCESS_KEY_ID" {
  type = string
  description = "Access Key ID"
}

variable "AWS_SECRET_ACCESS_KEY" {
  type = string
  description = "Access Key"
}